<?php

namespace App\Exports;

use App\Models\Conference;
use App\Models\UserConference;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ReportExport implements FromView {
  /**
  * @return \Illuminate\Support\Collection
  */
  // public function collection() {
  //   return Conference::all();
  // }

  public function view(): View {
      return view('exports.report', [ 'users' => UserConference::all(), 'conferences' => Conference::all()]);
  }
}
