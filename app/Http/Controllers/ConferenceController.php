<?php

namespace App\Http\Controllers;

use App\Models\Conference;
use App\Models\ViewConference;
use App\Models\UserConference;
use Illuminate\Http\Request;
use Cookie;

class ConferenceController extends Controller {

  /**
   * Display a listing of the resource. 
   */
  public function index() {
    $conferences = Conference::all();
  
    return view('conferences.index', compact('conferences'));
  }


  /**
   * Display the specified resource. 
   */
  public function show($id) {
    $conference = Conference::find($id);

    if (Cookie::has('user_logged')) {
      $user = UserConference::where('email', '=', Cookie::get('user_logged'))->first();

      $view = new ViewConference();
      $view->user_conference_id = $user->id;
      $view->conference_id = $id;
      $view->save();
      
      return view('conferences.show', compact('conference'));
    } else {
      return view('users.index');
    }
    
    
  }
}
