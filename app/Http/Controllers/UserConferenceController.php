<?php

namespace App\Http\Controllers;

use App\Models\UserConference;
use Illuminate\Http\Request;
use Cookie;
use Session;

class UserConferenceController extends Controller {
  /**
   * Display a listing of the resource. 
   */
  public function index() {
    return view('user_conferences.index');
  }

  /**
   * Store a newly created resource in storage. 
   */
  public function store(Request $request) {
    $this->validate($request, array(
      'name' => 'required',
      'lastname' => 'required',
      'company' => 'required',
      'email' => 'required|email|unique:user_conferences,email',
      'position' => 'required',
      'address' => 'required',
      'city' => 'required',
      'phone' => 'required'
    ));

    $user = new UserConference();
    $user->name = $request->input('name');
    $user->lastname = $request->input('lastname');
    $user->company = $request->input('company');
    $user->email = $request->input('email');
    $user->position = $request->input('position');
    $user->address = $request->input('address');
    $user->city = $request->input('city');
    $user->phone = $request->input('phone');
    $user->save();

    Session::flash('messages','El usuario ha sido creado.');
    Session::flash('class', 'alert-success'); 

    return back()->withInput();
  }

  /**
   * Method for logged users
   */
  public function session_conference(Request $request) {

    $this->validate($request, array(
      'email' => 'required|email',
    ));

    $user = UserConference::where('email', '=', $request->get('email'))->first();

    if ($user != null) {
      Cookie::queue('user_logged', $user->email, 2);
    } else {
      Session::flash('messages','Este correo aun no se ha registrado.');
      Session::flash('class', 'alert-warning'); 
    }
    return back()->withInput();
  }
}
