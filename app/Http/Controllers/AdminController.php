<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserConference;
use App\Models\Conference;


class AdminController extends Controller {

  /**
   * Create a new controller instance. 
   */
  public function __construct() {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard. 
   */
  public function index() {
    $users = UserConference::all();
    $conferences = Conference::all();

    return view('admin', compact('users', 'conferences'));
  }
}
