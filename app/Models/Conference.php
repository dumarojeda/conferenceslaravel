<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conference extends Model {
  public function views () {
    return $this->hasMany(ViewConference::class);
  }
}
