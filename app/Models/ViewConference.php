<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewConference extends Model {
  public function user () {
    return $this->belongsTo(UserConference::class, 'user_conference_id');
  }

  public function conference () {
    return $this->belongsTo(Conference::class, 'conference_id');
  }
}
