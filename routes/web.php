<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ConferenceController@index');

Route::resource('conferences', 'ConferenceController')->only(['index', 'show']);
Route::resource('user_conferences', 'UserConferenceController')->only(['index', 'store']);
Route::get('/session_conference', 'UserConferenceController@session_conference');

Auth::routes(['verify' => true, 'register' => false]);

Route::get('/admin', 'AdminController@index')->name('admin');

Route::get('/export', 'ExcelController@export');
