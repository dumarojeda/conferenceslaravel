
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

require('jquery-validation');

$(document).ready(function () {
  $('#register-form').validate({ 
      rules: {
        name: {
          required: true,
          maxlength: 60
        },
        lastname: {
          required: true,
          maxlength: 60
        },
        company: {
          required: true,
          maxlength: 60
        },
        email: {
          required: true,
          email: true
        },
        position: {
          required: true,
          maxlength: 60
        },
        address: {
          required: true,
          maxlength: 60
        },
        city: {
          required: true,
          maxlength: 60
        },
        phone: {
          required: true,
          digits: true  
        }
      },
      messages: {
        name: {
          required: "Debes escribir tu nombre.",
          maxlength: "No debes escribir mas de 60 caracteres."
        },
        lastname: {
          required: "Debes escribir tu apellido.",
          maxlength: "No debes escribir mas de 60 caracteres."
        },
        company: {
          required: "Debes escribir el nombre de tu empresa.",
          maxlength: "No debes escribir mas de 60 caracteres."
        },
        email: {
          required: "Debes escribir tu e-mail.",
          email: "Debes escribir un e-mail valido."
        },
        position: {
          required: "Debes escribir tu cargo.",
          maxlength: "No debes escribir mas de 60 caracteres."
        },
        address: {
          required: "Debes escribir tu dirección.",
          maxlength: "No debes escribir mas de 60 caracteres."
        },
        city: {
          required: "Debes escribir tu ciudad.",
          maxlength: "No debes escribir mas de 60 caracteres."
        },
        phone: {
          required: "Debes escribir tu teléfono.",
          digits: "Solo puedes escribir numeros."  
        }
      }
  });

  $('#login-form').validate({ 
      rules: {
        email: {
          required: true,
          email: true
        }
      },
      messages: {
        email: {
          required: "Debes escribir tu e-mail.",
          email: "Debes escribir un e-mail valido."
        }
      }
  });
});