@extends('layouts.app')

@section('title', 'Regístrate')

@section('content')
  <div class="row">
    <div class="col-lg-7 offset-lg-5 col-md-8 offset-md-4">
      <div class="row">
        <div class="col-md-12">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          @if (Session::has('messages'))
            <div class="alert {{ Session::get('class') }}">
              {{ Session::get('messages') }}
            </div>
          @endif
          <form class="form-group form-users" id="register-form" method="POST" action="{{ route('user_conferences.store') }}">
            @csrf
            <div class="row">
              <div class="col-md-6">  
                <div class="form-group">
                  <input type="text" name="name" class="form-control" placeholder="Nombre." />
                </div>
                <div class="form-group">
                  <input type="text" name="company" class="form-control" placeholder="Empresa." />
                </div>
                <div class="form-group">
                  <input type="text" name="position" class="form-control" placeholder="Cargo." />
                </div>
                <div class="form-group">
                  <input type="text" name="city" class="form-control" placeholder="Ciudad." />
                </div>
                <a href="#" class="btn btn-red btn-block btn-login-inside" data-toggle="modal" data-target="#loginModal">Ingresa</a>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" name="lastname" class="form-control" placeholder="Apellido." />
                </div>
                <div class="form-group">
                  <input type="text" name="email" class="form-control" placeholder="e-mail." />
                </div>
                <div class="form-group">
                  <input type="text" name="address" class="form-control" placeholder="Dirección." />
                </div>
                <div class="form-group">
                  <input type="text" name="phone" class="form-control" placeholder="Teléfono." />
                </div>
                <button type="submit" class="btn btn-red btn-block">Regístrate</button>
              </div>
            </div>
          </form>
        </div>

        <div class="col-sm-12">
          <a href="#" class="btn btn-red btn-block btn-login-outside" data-toggle="modal" data-target="#loginModal">Ingresa</a>
        </div>

        <div class="col-md-6 offset-md-6 col-sm-12">
          <a href="{{ url('/') }}" class="btn btn-red btn-block">Regresa al menu</a>
        </div>
      </div>
    </div>
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">              
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <div class="description">Escribe tu correo electrónico para poder ingresar.</div>
                <form class="form-group" id="login-form"  method="GET" action="{{ action('UserConferenceController@session_conference') }}">
                  <div class="row">
                    <div class="col-md-12"> 
                      <div class="form-group">
                        <input type="text" name="email" class="form-control" />
                      </div>
                    </div>
                    <div class="col-md-8 offset-md-4">
                      <button type="submit" class="btn btn-red btn-block">Ingresa</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection