@extends('layouts.app')

@section('title', 'Restablecer la contraseña')

@section('content')

<div class="row">
  <div class="col-xl-4 offset-xl-4 col-lg-8 offset-lg-2 col-md-10 offset-md-1">
    <h1 class="description">Restablecer la contraseña</h1>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    @if (session('status'))
      <div class="alert alert-success" role="alert">
        {{ session('status') }}
      </div>
    @endif

    <form method="POST" action="{{ route('password.email') }}">
      @csrf

      <div class="form-group">
        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-mail">
      </div>

      <div class="form-group">
        <button type="submit" class="btn btn-red btn-block">
          {{ __('Enviar link.') }}
        </button>
      </div>
    </form>
  </div>
</div>
@endsection
