@extends('layouts.app')

@section('title', 'Administrador')

@section('content')

<div class="row">
  <div class="col-xl-4 offset-xl-4 col-lg-8 offset-lg-2 col-md-10 offset-md-1">
    <h1 class="description">Administrador</h1>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <form method="POST" action="{{ route('login') }}">
      @csrf
      <div class="form-group">
        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-mail" autofocus>
      </div>

      <div class="form-group">
        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Contraseña">
      </div>

      <div class="form-group">
        <div class="form-check">
          <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

          <label class="form-check-label" for="remember">
            {{ __('Recuerdame') }}
          </label>
        </div>
      </div>

      <div class="form-group">
        <button type="submit" class="btn btn-red btn-block">
          {{ __('Iniciar sesión') }}
        </button>
      </div>

      <div class="form-group">
        @if (Route::has('password.request'))
          <a class="btn btn-link" href="{{ route('password.request') }}">
            {{ __('¿Olvidaste tu contraseña?') }}
          </a>
        @endif
      </div>
    </form>
  </div>
</div>
@endsection
