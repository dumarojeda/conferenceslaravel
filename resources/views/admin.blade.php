@extends('layouts.app')

@section('title', 'Administrador')

@section('content')
  <h2 class="description">Hola Administrador, 
    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
      {{ __('Cerrar Sesión') }}
    </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
      @csrf
    </form>
  </h2>

  @if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
  </div>
  @endif

  <h3 class="main-title">Reporte de vistas de videos por usuario.</h3>

  @foreach($users as $user)
    <div>
      <h3 class="subtitle">{{$user->name}} ha visto {{$user->views->count()}} conferencias</h3>
      <ul>
          @foreach($conferences as $conference)
            @php
              $viewsByConference = 0
            @endphp
            @foreach($user->views as $view)
              @if ($view->conference->id == $conference->id)
                @php
                  $viewsByConference++
                @endphp
              @endif
            @endforeach
            <li>Conferencia de {{$conference->title}} fue vista {{$viewsByConference}} veces</li>
          @endforeach  
      </ul>
    </div>
  @endforeach
  <a href="/export" class="btn btn-red">Exportar reporte en Excel</a>

@endsection