@extends('layouts.app')

@section('title', 'Bienvenidos')

@section('content')
  <h2 class="description">Haz clic para entrar a cualquiera  de nuestras conferencias en directo.</h2>
  <div class="conferences">
    <div class="row justify-content-center">
      @foreach($conferences as $conference)
        <div class="col-lg-4 col-md-6 col-sm-12">
          <div class="conference-item">
            <a href="/conferences/{{$conference->id}}">
              <h3 class="title">{{$conference->title}}</h3>
              <img src="http://i3.ytimg.com/vi/{{$conference->id_video}}/hqdefault.jpg" alt="{{$conference->title}}" class="image" />
              <div class="play-icon"></div>
            </a>
          </div>
        </div>
      @endforeach
    </div>
  </div>

@endsection