@extends('layouts.app')

@section('title', $conference->title)

@section('content')
  <div class="row">
    <div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1">
      <div class="conference-single">
        <h2 class="title">{{$conference->title}}</h2>
        <div class="video">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$conference->id_video}}?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>
    <div class="col-md-4 offset-md-8">
      <a href="{{ url('/') }}" class="btn btn-red btn-block">Regresa al menu</a>
    </div>
  </div>
@endsection