<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Conferences Laravel - @yield('title')</title>
	<link rel="stylesheet" href="{{ asset('css/app.css') }}" />
</head>
<body>
  <section id="wrapper-header">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="logo">
            <span class="main">
              Oracle
            </span>
            Digital <b>Transformation</b> Day
          </div>
        </div>
        <div class="col-md-6">
          <h1 class="main-title">@yield('title')</h1>
        </div>
      </div>
    </div>
  </section>
  <section id="wrapper-content">
  	<div class="container">
  		@yield('content')
  	</div>
  </section>
  <section id="wrapper-footer">
    <div class="container">
      #OracleDigital
    </div>
  </section>
	<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</body>
</html>