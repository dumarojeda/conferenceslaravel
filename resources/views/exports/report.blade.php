<table>
  <thead>
  <tr>
    <th>ID</th>
    <th>Usuario</th>
    <th>Conferencias vistas</th>
    @foreach($conferences as $conference)
      <th>{{$conference->title}}</th>
    @endforeach
  </tr>
  </thead>
  <tbody>
    @foreach($users as $user)
      <tr>
          <td>{{ $user->id }}</td>
          <td>{{ $user->name }}</td>
          <td>{{ $user->views->count() }}</td>
          @foreach($conferences as $conference)
            @php
              $viewsByConference = 0
            @endphp
            @foreach($user->views as $view)
              @if ($view->conference->id == $conference->id)
                @php
                  $viewsByConference++
                @endphp
              @endif
            @endforeach
            <td>{{$viewsByConference}}</td>
          @endforeach  
      </tr>
    @endforeach
  </tbody>
</table>