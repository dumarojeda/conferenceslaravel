<?php

use Illuminate\Database\Seeder;
use App\Models\Conference;

class ConferencesTableSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    Conference::create(['title' => 'Proceso de Transformación Digital', 'id_video' => '9uyyOHIidMA']);
    Conference::create(['title' => 'Recursos humanos', 'id_video' => 'mkggXE5e2yk']);
    Conference::create(['title' => 'Mercadeo - Ventas y Servicio al cliente', 'id_video' => 'YKvpxZm1ryY']);
    Conference::create(['title' => 'Sector público', 'id_video' => 'MqBNI8ZPczQ']);
    Conference::create(['title' => 'Optimización empresarial y analiticos', 'id_video' => 'vyN84xI8OKQ']);
  }
}
