# Conferences Laravel

Este proyecto consta de una plataforma para ver conferencias unicamente para usuarios registrados, realiza seguimiento de quien ve un video y cuantas veces lo ve.

## Diseño de la base de datos

Se tienen tres tablas para las bases de datos:

  * conferences: Esta tabla tiene el titulo y el id del video de youtube.
  * user_conferences: Esta tabla tiene nombre, apellido, empresa, email, cargo, dirección, ciudad y teléfono.
  * views: Esta tabla se asocia con las anteriores tablas, de esta forma se puede relacionar una vista con el id de un usuario y el id de un video.

 [Diseño de base de datos](https://www.dropbox.com/s/nbo848584xu9724/db_conferenceslaravel.png?dl=0).

## Como instalar el proyecto
* Primero debemos clonar el repositorio.  
`git clone git@gitlab.com:dumarojeda/conferenceslaravel.git`
* Luego se debe acceder al folder.  
`cd conferenceslaravel`
* Después se deben instalar todas las dependencias de Composer.  
`composer install`
* Hay que asegurarse de tener mysql encendido, crear una base de datos vacia y configurar el archivo *.env* según las especificaciones del sistema.
* Luego se deben correr las migraciones.  
`php artisan migrate`
* Después se corren los seeds para crear las conferencias y el usuario administrador.  
`php artisan db:seed --class=ConferencesTableSeeder`.  
`php artisan db:seed --class=UsersTableSeeder`
* Ya esta la aplicación lista se puede correr el servidor sin ningún problema.  
`php artisan serve`
* Ahora puedes entrar a [http://127.0.0.1:8000/](http://127.0.0.1:8000/)

### Credenciales para el administrador
Puedes entrar en [http://127.0.0.1:8000/login](http://127.0.0.1:8000/login)
* Email: admin@example.com
* Contraseña: 12345

Eso es todo, muchas gracias!

#### Proyecto realizado por Dumar Ojeda.